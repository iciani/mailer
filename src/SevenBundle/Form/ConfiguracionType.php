<?php

namespace SevenBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;

use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Security\Core\Security;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class ConfiguracionType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('host', TextType::class, [
                    'label' => 'SMTP Host',
                    'label_attr' => ['icon' => 'envelope-o'],
                    'required' => true,
                    'attr' => ['width' => 'col-sm-6 col-md-6 col-lg-6']
                ])
                ->add('port', IntegerType::class, [
                    'label' => 'Port',
                    'label_attr' => ['icon' => 'info'],
                    'required' => true,
                    'attr' => ['width' => 'col-sm-6 col-md-6 col-lg-6']
                ])
                ->add('user', EmailType::class, [
                    'label' => 'Usuario',
                    'label_attr' => ['icon' => 'user'],
                    'attr' => ['width' => 'col-sm-6 col-md-6 col-lg-6']
                ])
                ->add('password', TextType::class, [
                    'label' => 'Contraseña',
                    'label_attr' => ['icon' => 'lock'],
                    'required' => true,
                    'attr' => ['width' => 'col-sm-6 col-md-6 col-lg-6']
                ])
                ->add('encryption', ChoiceType::class, [
                 'choices' => array(
                                     'SSL' => 'ssl',
                                     'TLS' => 'tls'
                                    ),
                'choices_as_values' => true, // switched
                'label' => 'Encriptación',
                'label_attr' => ['icon' => 'user'],
                'attr' => ['width' => 'col-sm-4 col-md-4 col-lg-4', 'data-toggle' => 'chosen'],
                'required' => true,
                ])                
                ->add('betweenTime', IntegerType::class, [
                    'label' => 'Demora entre cada Mail (Segundos)',
                    'label_attr' => ['icon' => 'refresh'],
                    'required' => true,
                    'attr' => [ 'width' => 'col-sm-4 col-md-4 col-lg-4']
                ])
                ->add('maxMailPerDay', IntegerType::class, [
                    'label' => 'Número Máximo de mails por Dia',
                    'label_attr' => ['icon' => 'sort-numeric-desc'],
                    'required' => true,
                    'attr' => ['width' => 'col-sm-4 col-md-4 col-lg-4']
                ])                
                // ->add('randomTime', CheckboxType::class, [
                //     'attr' => [ 'width' => 'col-sm-6 col-md-6 col-lg-6', 
                //                 'data-toggle' => 'checkbox', 
                //                 'class' => 'bootstrap_checkbox' ],
                //     'required' => false,
                //     'label' => 'Timpo Aleatorio entre Mails'
                // ])              
                ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'SevenBundle\Entity\Configuracion'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'sevenbundle_configuracion';
    }


}
