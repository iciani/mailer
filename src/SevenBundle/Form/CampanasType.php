<?php

namespace SevenBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;

use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Security\Core\Security;
use Doctrine\Common\Collections\ArrayCollection;

class CampanasType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('titulo', TextType::class, [
                    'label' => 'Título de la Campaña',
                    'label_attr' => ['icon' => 'share-square'],
                    'required' => true,
                    'attr' => [ 'width' => 'col-sm-12 col-lg-12']
                ]) 
                ->add('subject', TextType::class, [
                    'label' => 'Asunto del Mail',
                    'label_attr' => ['icon' => 'user'],
                    'required' => true,
                    'attr' => [ 'width' => 'col-sm-12 col-lg-12']
                ])
                ->add('sender', TextType::class, [
                    'label' => 'Texto Remitente',
                    'label_attr' => ['icon' => 'envelope-o'],
                    'required' => true,
                    'attr' => [ 'width' => 'col-sm-12 col-lg-12']
                ])        
                ->add('html', TextareaType::class, [
                    'label' => false,
                    'attr' => [ 'data-toggle' => 'summernote'],
                    'required' => true
                ])
                ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'SevenBundle\Entity\Campanas'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'sevenbundle_campanas';
    }


}
