<?php

namespace SevenBundle\Controller;

use SevenBundle\Entity\Configuracion;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Configuracion controller.
 *
 * @Route("configuracion")
 */
class ConfiguracionController extends Controller
{
    /**
     * Displays a form to edit an existing configuracion entity.
     *
     * @Route("/{id}/edit", name="configuracion_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Configuracion $entity)
    {
        $editForm = $this->createForm('SevenBundle\Form\ConfiguracionType', $entity);
        $editForm->handleRequest($request);
        $flashbagService = $this->get('flashbag_service');
        $translator = $this->get('translator');

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $flashbagService->setFlash('success', $translator->trans('core.controller.messages.update_success', [], 'CoreBundle'));
            return $this->redirectToRoute('configuracion_edit', array('id' => $entity->getId()));
        }

        return $this->render('configuracion/edit.html.twig', array(
            'configuracion' => $entity,
            'edit_form' => $editForm->createView(),
        ));
    }
}
