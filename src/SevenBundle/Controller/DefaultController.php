<?php

namespace SevenBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {
        $em         = $this->getDoctrine()->getManager();
        $campanas   = count($em->getRepository('SevenBundle:Campanas')->findAll());
        $mails      = count($em->getRepository('SevenBundle:Mails')->findAll());
        $controlLog = $em->getRepository('SevenBundle:ControlLog')->findOneBy(['id' => 1]);

        $activa     = $controlLog->getActiva();
        $porc       = $controlLog->getPorcEnviado();

        return $this->render('SevenBundle:Default:index.html.twig', array(
            'campanas' => $campanas,
            'mails'    => $mails,
            'activa'   => $activa,
            'porc'     => $porc,
        ));
    }
}
