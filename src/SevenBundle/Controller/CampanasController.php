<?php

namespace SevenBundle\Controller;

use SevenBundle\Entity\Campanas;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\File\UploadedFile;


/**
 * Campana controller.
 *
 * @Route("campanas")
 */
class CampanasController extends Controller
{
    /**
     * Lists all campana entities.
     *
     * @Route("/", name="campanas_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('SevenBundle:Campanas')->findAll();

        //Recuperamos la tabla de Log. 
        $controlLog = $em->getRepository('SevenBundle:ControlLog')->findOneBy(['id' => 1]);

        return $this->render('campanas/index.html.twig', array(
            'entities'       => $entities,
            'campanaActiva'  => $controlLog->getCampanaActiva(),
            'campanaProgres' => $controlLog->getPorcEnviado(),
        ));
    }

    /**
     * Inicia una Campana.
     *
     * @Route("/init", options={"expose"=true}, name="campanas_init")
     * @Method({"GET", "POST"})
     */
    public function ajaxInitAction(Request $request)
    {
        $em    = $this->getDoctrine()->getManager();
        $param = $request->request->all();
        
        //Recuperamos la campaña que se selecciono para activar.
        $campana    = $em->getRepository('SevenBundle:Campanas')->findOneBy(['id' => $param['campana'] ]);
        
        //Recuperamos la lista de mails a enviar.
        $mails      = $em->getRepository('SevenBundle:Mails')->findAll();
        $countMails = count($mails);

        //Recuperamos la tabla de Log. Si hay una campañana Activa, no se puede iniciar otra. 
        $controlLog = $em->getRepository('SevenBundle:ControlLog')->findOneBy(['id' => 1]);

        if($controlLog->getActiva() == 0){
            
            $controlLog->setActiva(1); 
            $controlLog->setMailsEnviar($countMails); 
            $controlLog->setMailsEnviados(0); 
            $controlLog->setPorcEnviado(0); 
            $controlLog->setUltimoUpd(new \DateTime()); 
            $controlLog->setCampanaActiva($campana); 
            $em->persist($controlLog);
            $em->flush($controlLog);

            //LANZAMOS COMANDO ASYNCRONO EN EL SERVIDOR.
            shell_exec("/usr/bin/nohup php bin/console campana:init >/dev/null 2>&1 &");
                                        
            return new JsonResponse('success', Response::HTTP_OK);
        }else{
            return new JsonResponse('error', Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Creates a new campana entity.
     *
     * @Route("/new", name="campanas_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $entity = new Campanas();
        $form = $this->createForm('SevenBundle\Form\CampanasType', $entity);
        $form->handleRequest($request);
        $flashbagService = $this->get('flashbag_service');
        $translator = $this->get('translator');

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush($entity);

            $flashbagService->setFlash('success', $translator->trans('core.controller.messages.creation_success', [], 'CoreBundle'));
            return $this->redirectToRoute('campanas_show', array('id' => $entity->getId()));
        }

        return $this->render('campanas/new.html.twig', array(
            'campana' => $entity,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a campana entity.
     *
     * @Route("/{id}", name="campanas_show")
     * @Method("GET")
     */
    public function showAction(Campanas $entity)
    {
        $deleteForm = $this->createDeleteForm($entity);

        return $this->render('campanas/show.html.twig', array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing campana entity.
     *
     * @Route("/{id}/edit", name="campanas_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Campanas $entity)
    {
        $deleteForm = $this->createDeleteForm($entity);
        $editForm = $this->createForm('SevenBundle\Form\CampanasType', $entity);
        $editForm->handleRequest($request);
        $flashbagService = $this->get('flashbag_service');
        $translator = $this->get('translator');

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $flashbagService->setFlash('success', $translator->trans('core.controller.messages.update_success', [], 'CoreBundle'));
            return $this->redirectToRoute('campanas_edit', array('id' => $entity->getId()));
        }

        return $this->render('campanas/edit.html.twig', array(
            'campana' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a campana entity.
     *
     * @Route("/{id}", name="campanas_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Campanas $campana)
    {
        $form = $this->createDeleteForm($campana);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($campana);
            $em->flush();
        }

        return $this->redirectToRoute('campanas_index');
    }

    /**
     * Creates a form to delete a campana entity.
     *
     * @param Campanas $campana The campana entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Campanas $campana)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('campanas_delete', array('id' => $campana->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
