<?php

namespace SevenBundle\Controller;

use SevenBundle\Entity\Test;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Test controller.
 *
 * @Route("test")
 */
class TestController extends Controller
{
    /**
     * Lists all test entities.
     *
     * @Route("/", name="test_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        return $this->render('test/index.html.twig', array(
        //     'entities' => $entities,
        ));
    }

    /**
     * Creates a new test entity.
     *
     * @Route("/new", name="test_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $entity = new Test();
        $form = $this->createForm('SevenBundle\Form\TestType', $entity);
        $form->handleRequest($request);
        $flashbagService = $this->get('flashbag_service');
        $translator = $this->get('translator');

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush($entity);

            $flashbagService->setFlash('success', $translator->trans('core.controller.messages.creation_success', [], 'CoreBundle'));
            return $this->redirectToRoute('test_show', array('id' => $entity->getId()));
        }

        return $this->render('test/new.html.twig', array(
            'test' => $entity,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a test entity.
     *
     * @Route("/{id}", name="test_show")
     * @Method("GET")
     */
    public function showAction(Test $entity)
    {
        $deleteForm = $this->createDeleteForm($entity);

        return $this->render('test/show.html.twig', array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing test entity.
     *
     * @Route("/{id}/edit", name="test_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Test $entity)
    {
        $em = $this->getDoctrine()->getManager();
        $deleteForm = $this->createDeleteForm($entity);
        $editForm = $this->createForm('SevenBundle\Form\TestType', $entity);
        $editForm->handleRequest($request);
        $flashbagService = $this->get('flashbag_service');
        $translator = $this->get('translator');
        $i = 0;
        
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            
            $this->getDoctrine()->getManager()->flush();
            
            //Recuperamos la Configuracion del servidor de correos. 
            $config = $em->getRepository('SevenBundle:Configuracion')->findOneBy(['id' => 1]);

            //Recuperamos la lista de mails a enviar.
            $mails = $em->getRepository('SevenBundle:Mails')->findAll();

            $transport = (new \Swift_SmtpTransport($config->getHost(), $config->getPort()))
            ->setUsername($config->getUser()) //username from database
            ->setPassword($config->getPassword()) //password from database
            ->setEncryption($config->getEncryption());

            // Create the Mailer using your created Transport
            $mailer = new \Swift_Mailer($transport);

            foreach ($mails as $mail) {
                
                $subject = $entity->getSubject();
                $subject = str_replace('{%nombre%}', $mail->getNombre(), $subject);
                $message = (new \Swift_Message($subject))
                    ->setFrom('Leticia')
                    ->setTo($mail->getMail())
                    ->setBody($entity->getHtml(), 'text/html');
                //$this->get('mailer')->send($message);
                $mailer->send($message);
                $i += 1;
            }

            $flashbagService->setFlash('success', $i . ' Correos enviados.');
            return $this->redirectToRoute('test_edit', array('id' => $entity->getId()));
        }

        return $this->render('test/edit.html.twig', array(
            'test' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a test entity.
     *
     * @Route("/{id}", name="test_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Test $test)
    {
        $form = $this->createDeleteForm($test);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($test);
            $em->flush();
        }

        return $this->redirectToRoute('test_index');
    }

    /**
     * Creates a form to delete a test entity.
     *
     * @param Test $test The test entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Test $test)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('test_delete', array('id' => $test->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
