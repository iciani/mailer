<?php

namespace SevenBundle\Controller;

use SevenBundle\Entity\Mails;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Mail controller.
 *
 * @Route("mails")
 */
class MailsController extends Controller
{
    /**
     * Lists all mail entities.
     *
     * @Route("/", name="mails_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('SevenBundle:Mails')->findAll();

        return $this->render('mails/index.html.twig', array(
            'entities' => $entities,
        ));
    }

    /**
     * Creates a new mail entity.
     *
     * @Route("/new", name="mails_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $entity = new Mails();
        $form = $this->createForm('SevenBundle\Form\MailsType', $entity);
        $form->handleRequest($request);
        $flashbagService = $this->get('flashbag_service');
        $translator = $this->get('translator');

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush($entity);

            $flashbagService->setFlash('success', $translator->trans('core.controller.messages.creation_success', [], 'CoreBundle'));
            return $this->redirectToRoute('mails_show', array('id' => $entity->getId()));
        }

        return $this->render('mails/new.html.twig', array(
            'mail' => $entity,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a mail entity.
     *
     * @Route("/{id}", name="mails_show")
     * @Method("GET")
     */
    public function showAction(Mails $entity)
    {
        $deleteForm = $this->createDeleteForm($entity);

        return $this->render('mails/show.html.twig', array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing mail entity.
     *
     * @Route("/{id}/edit", name="mails_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Mails $entity)
    {
        $deleteForm = $this->createDeleteForm($entity);
        $editForm = $this->createForm('SevenBundle\Form\MailsType', $entity);
        $editForm->handleRequest($request);
        $flashbagService = $this->get('flashbag_service');
        $translator = $this->get('translator');

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $flashbagService->setFlash('success', $translator->trans('core.controller.messages.update_success', [], 'CoreBundle'));
            return $this->redirectToRoute('mails_edit', array('id' => $entity->getId()));
        }

        return $this->render('mails/edit.html.twig', array(
            'mail' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a mail entity.
     *
     * @Route("/{id}", name="mails_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Mails $mail)
    {
        $form = $this->createDeleteForm($mail);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($mail);
            $em->flush();
        }

        return $this->redirectToRoute('mails_index');
    }

    /**
     * Creates a form to delete a mail entity.
     *
     * @param Mails $mail The mail entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Mails $mail)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('mails_delete', array('id' => $mail->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
