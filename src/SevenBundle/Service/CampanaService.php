<?php

namespace SevenBundle\Service;

use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

use Symfony\Component\HttpFoundation\Session\Session;   
use SevenBundle\Entity\Campana;
use UserBundle\Entity\User;

/**
 * Use with "campana.service"
 */
class CampanaService
{   
    protected $em;

	public function __construct(EntityManager $entityManager, TokenStorage $tokenStorage)
    {
        $this->em = $entityManager;
    }


}