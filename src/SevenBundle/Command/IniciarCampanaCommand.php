<?php
namespace SevenBundle\Command;

use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use SevenBundle\Service\CampanaService;

class IniciarCampanaCommand extends ContainerAwareCommand
{

    public function __construct()
    {
       parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('campana:init')
            ->setDescription('Iniciando Campaña Seleccionada');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $date = new \DateTime();
        $em   = $this->getContainer()->get('doctrine')->getManager();
        $i    = 0;

        //Recuperamos la tabla de Log. Si hay una campañana Activa, no se puede iniciar otra. 
        $controlLog = $em->getRepository('SevenBundle:ControlLog')->findOneBy(['id' => 1]);

        //Recuperamos la Configuracion del servidor de correos. 
        $config     = $em->getRepository('SevenBundle:Configuracion')->findOneBy(['id' => 1]);

        //Recuperamos la campaña que se selecciono para activar.
        $campana    = $em->getRepository('SevenBundle:Campanas')->findOneBy(['id' => $controlLog->getCampanaActiva()->getId()]);

        //Recuperamos la lista de mails a enviar.
        $mails      = $em->getRepository('SevenBundle:Mails')->findAll();
        $countMails = count($mails);

        $transport = (new \Swift_SmtpTransport($config->getHost(), $config->getPort()))
        ->setUsername($config->getUser()) //username from database
        ->setPassword($config->getPassword()) //password from database
        ->setEncryption($config->getEncryption());

        // Create the Mailer using your created Transport
        $mailer = new \Swift_Mailer($transport);

        foreach ($mails as $mail) {
            
            $subject = $campana->getSubject();
            $subject = str_replace('{&nombre&}', $mail->getNombre(), $subject);
            $message = (new \Swift_Message($subject))
                ->setFrom($campana->getSender())
                ->setTo($mail->getMail())
                ->setBody($campana->getHtml(), 'text/html');
            //$this->get('mailer')->send($message);
            $mailer->send($message);
            $i += 1;

            if($config->getBetweenTime() != 0){
                sleep($config->getBetweenTime());
            }

            if($i == $config->getMaxMailPerDay()){
                sleep(95000);
            }

            $controlLog->setMailsEnviados($i); 
            if($countMails <> 0){
                $controlLog->setPorcEnviado($i * 100 / $countMails); 
            }else{
                $controlLog->setPorcEnviado(100); 
            }
            $controlLog->setUltimoUpd(new \DateTime()); 
            $em->persist($controlLog);
            $em->flush($controlLog);

        }
   
        $controlLog->setActiva(0); 
        $controlLog->setMailsEnviados($i); 
        $controlLog->setPorcEnviado(100); 
        $controlLog->setUltimoUpd(new \DateTime()); 
        $controlLog->setCampanaActiva(null);
        $em->persist($controlLog);
        $em->flush($controlLog);

        $output->writeln($date->format('d/m/Y h:i:s'). ' - '.'Campaña Finalizada.');

    }

}