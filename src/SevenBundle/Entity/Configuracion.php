<?php

namespace SevenBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Configuracion
 *
 * @ORM\Table(name="configuracion")
 * @ORM\Entity(repositoryClass="SevenBundle\Repository\ConfiguracionRepository")
 */
class Configuracion
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="betweenTime", type="integer")
     */
    private $betweenTime;

    /**
     * @var int
     *
     * @ORM\Column(name="maxMailPerDay", type="integer")
     */
    private $maxMailPerDay;

    /**
     * @var bool
     *
     * @ORM\Column(name="randomTime", type="boolean")
     */
    private $randomTime;

    /**
     * @var string
     *
     * @ORM\Column(name="host", type="string", length=50)
     */
    private $host;

    /**
     * @var string
     *
     * @ORM\Column(name="port", type="integer")
     */
    private $port;

    /**
     * @var string
     *
     * @ORM\Column(name="encryption", type="string", length=3)
     */
    private $encryption;

    /**
     * @var string
     *
     * @ORM\Column(name="user", type="string", length=50)
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=50)
     */
    private $password;



    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getBetweenTime()
    {
        return $this->betweenTime;
    }

    /**
     * @param int $betweenTime
     *
     * @return self
     */
    public function setBetweenTime($betweenTime)
    {
        $this->betweenTime = $betweenTime;

        return $this;
    }

    /**
     * @return bool
     */
    public function isRandomTime()
    {
        return $this->randomTime;
    }

    /**
     * @param bool $randomTime
     *
     * @return self
     */
    public function setRandomTime($randomTime)
    {
        $this->randomTime = $randomTime;

        return $this;
    }

    /**
     * @return string
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * @param string $host
     *
     * @return self
     */
    public function setHost($host)
    {
        $this->host = $host;

        return $this;
    }

    /**
     * @return string
     */
    public function getPort()
    {
        return $this->port;
    }

    /**
     * @param string $port
     *
     * @return self
     */
    public function setPort($port)
    {
        $this->port = $port;

        return $this;
    }

    /**
     * @return string
     */
    public function getEncryption()
    {
        return $this->encryption;
    }

    /**
     * @param string $encryption
     *
     * @return self
     */
    public function setEncryption($encryption)
    {
        $this->encryption = $encryption;

        return $this;
    }

    /**
     * @return string
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param string $user
     *
     * @return self
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     *
     * @return self
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return string
     */
    public function getMaxMailPerDay()
    {
        return $this->maxMailPerDay;
    }

    /**
     * @param string $maxMailPerDay
     *
     * @return self
     */
    public function setMaxMailPerDay($maxMailPerDay)
    {
        $this->maxMailPerDay = $maxMailPerDay;

        return $this;
    }
}
