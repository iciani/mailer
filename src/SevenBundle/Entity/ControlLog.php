<?php

namespace SevenBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ControlLog
 *
 * @ORM\Table(name="control_log")
 * @ORM\Entity(repositoryClass="SevenBundle\Repository\ControlLogRepository")
 */
class ControlLog
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var bool
     *
     * @ORM\Column(name="activa", type="boolean")
     */
    private $activa;

    /**
     * @var int
     *
     * @ORM\Column(name="mailsEnviar", type="integer")
     */
    private $mailsEnviar;

    /**
     * @var int
     *
     * @ORM\Column(name="mailsEnviados", type="integer")
     */
    private $mailsEnviados;

    /**
     * @var int
     *
     * @ORM\Column(name="porcEnviado", type="integer")
     */
    private $porcEnviado;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ultimoUpd", type="datetime")
     */
    private $ultimoUpd;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="SevenBundle\Entity\Campanas")
     * @ORM\JoinColumn(name="campanaActiva", referencedColumnName="id", nullable=true)
     */
    private $campanaActiva;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set activa.
     *
     * @param bool $activa
     *
     * @return ControlLog
     */
    public function setActiva($activa)
    {
        $this->activa = $activa;

        return $this;
    }

    /**
     * Get activa.
     *
     * @return bool
     */
    public function getActiva()
    {
        return $this->activa;
    }

    /**
     * Set mailsEnviar.
     *
     * @param int $mailsEnviar
     *
     * @return ControlLog
     */
    public function setMailsEnviar($mailsEnviar)
    {
        $this->mailsEnviar = $mailsEnviar;

        return $this;
    }

    /**
     * Get mailsEnviar.
     *
     * @return int
     */
    public function getMailsEnviar()
    {
        return $this->mailsEnviar;
    }

    /**
     * Set mailsEnviados.
     *
     * @param int $mailsEnviados
     *
     * @return ControlLog
     */
    public function setMailsEnviados($mailsEnviados)
    {
        $this->mailsEnviados = $mailsEnviados;

        return $this;
    }

    /**
     * Get mailsEnviados.
     *
     * @return int
     */
    public function getMailsEnviados()
    {
        return $this->mailsEnviados;
    }

    /**
     * Set porcEnviado.
     *
     * @param int $porcEnviado
     *
     * @return ControlLog
     */
    public function setPorcEnviado($porcEnviado)
    {
        $this->porcEnviado = $porcEnviado;

        return $this;
    }

    /**
     * Get porcEnviado.
     *
     * @return int
     */
    public function getPorcEnviado()
    {
        return $this->porcEnviado;
    }

    /**
     * Set ultimoUpd.
     *
     * @param \DateTime $ultimoUpd
     *
     * @return ControlLog
     */
    public function setUltimoUpd($ultimoUpd)
    {
        $this->ultimoUpd = $ultimoUpd;

        return $this;
    }

    /**
     * Get ultimoUpd.
     *
     * @return \DateTime
     */
    public function getUltimoUpd()
    {
        return $this->ultimoUpd;
    }

    /**
     * Set campanaActiva.
     *
     * @param string $campanaActiva
     *
     * @return ControlLog
     */
    public function setCampanaActiva($campanaActiva)
    {
        $this->campanaActiva = $campanaActiva;

        return $this;
    }

    /**
     * Get campanaActiva.
     *
     * @return string
     */
    public function getCampanaActiva()
    {
        return $this->campanaActiva;
    }
}
