<?php

namespace CoreBundle\Service;

use Symfony\Component\HttpFoundation\Session\Session;

class FlashBagService
{
    private $session;

    public function __construct(Session $session)
    {
        $this->session = $session;
    }

    public function setFlash($index, $message)
    {
        $this->session->getFlashBag()->clear();
        $this->session->getFlashBag()->add($index,$message);
    }

}