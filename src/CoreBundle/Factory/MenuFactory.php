<?php

namespace CoreBundle\Factory;

use Symfony\Component\Finder\Finder;
use Symfony\Component\Yaml\Parser;

use Symfony\Component\Security\Core\SecurityContext;

class MenuFactory
{
    protected $user;

    protected $roles;

    public function __construct(SecurityContext $context)
    {
        $this->user = $context->getToken()->getUser();
        $this->roles = $this->user->getRoles();
    }

    /**
     * Contruye el menu recorriendo los archivos menu.yml de todos los bundles
     *
     * @return []
     */
    public function buildMenu()
    {
        $dir = __DIR__. "/../..";
        $finder = new Finder();
        $finder->files()->name('menu.yml')->in($dir);

        $yaml = new Parser();
        $menu = [];
        foreach ($finder as $file) {
            $fileParsed = $yaml->parse(file_get_contents($file));
            foreach($fileParsed as $m) {
                $menu[$m['order']] = $m;
            }
        }
        ksort($menu);

        $this->rebuildWithPermissions($menu);

        return $menu;
    }

    private function rebuildWithPermissions(&$menu)
    {
        foreach($menu as $mKey => $m) {
            if (!$this->hasPermission($m)) {
                unset($menu[$mKey]);
            }
            if (isset($m['submenu'])) {
                foreach($m['submenu'] as $sKey => $s) {
                    if (!$this->hasPermission($s)) {
                        unset($menu[$mKey]['submenu'][$sKey]);
                        if (count($menu[$mKey]['submenu']) == 0) {
                            unset($menu[$mKey]);
                        }
                    }
                }
            }
        }
    }

    private function hasPermission($menu)
    {
        if (isset($menu['permissions'])) {
            foreach($menu['permissions'] as $permission) {
                if (in_array($permission, $this->roles)) {
                    return true;
                }
            }
            return false;
        } else {
            return true;
        }
    }
}