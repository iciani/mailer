var DataTableWidget = Class.extend({
    containerId: "#dataTable",
    url: "",
    dataTableOptions: {
        responsive: {
            details: {
                type: 'column'
            }
        },
        columnDefs: [ {
            className: 'control',
            orderable: false,
            targets:   0
        } ],
        order: [ 1, 'asc' ],
        oLanguage: {
            sProcessing   :  "",
            sLengthMenu   :  "Mostrar _MENU_ registros",
            sZeroRecords  :  "No se encontraron resultados",
            sInfo         :  "Mostrando desde _START_ hasta _END_ de _TOTAL_ registros",
            sInfoEmpty    :  "Mostrando desde 0 hasta 0 de 0 registros",
            sInfoFiltered :  "(filtrado de _MAX_ registros en total)",
            sInfoPostFix  :  "",
            sSearch       :  'Buscar: ',
            sUrl          :  "",
            oPaginate: {
                sFirst:    "Primero",
                sPrevious: "Anterior",
                sNext:     "Siguiente",
                sLast:     "Último"
            },

        },

        iDisplayLength: 10,

        bProcessing: false,

        bServerSide: false,

        sPaginationType: "full_numbers",

        sDom: "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
                        "t"+
             "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",

        autoWidth: true
    },

    render: function (nombre, url) {
        var widget = this;
        if (widget.dataTableOptions.bServerSide) {
            widget.dataTableOptions['sAjaxSource'] = widget.url;
        }

        widget.oTable = $(widget.containerId).dataTable(
            widget.dataTableOptions
        );
    },

    reload: function() {
        var self = this;
        self.oTable.ajax.reload();
    },

    deleteRow: function($row) {
        var self = this;
        self.oTable.fnDeleteRow( $row );
    },

    init: function(args) {
        var widget = this;
        for (var property in args) {
          if (args.hasOwnProperty(property)) {
            if (!$.isPlainObject(args[property])) {
                widget[property] = args[property];
            } else {
                for(var o in args[property]) {
                    if (args[property].hasOwnProperty(o)) {
                        widget[property][o] = args[property][o];
                    }
                }
            }
          }
        }

        widget.render();
    }
});

var FilesActionWidget = Class.extend({
    classButtonDelete: '.btn-delete',
    restoreAjax: function(entityId) {
        var self = this;
        $.ajax({
            type: 'post',
            url: self.urlRestore,
            data: {
                id: entityId
            },
            success: function() {
                $("a[data-entity-id=" + entityId + "]").attr('deleted', 0);
                $.smallBox({
                    title : "Restaurar",
                    content : "<i class='fa fa-clock-o'></i> <i>Restauración exitosa!</i>",
                    color : "#659265",
                    iconSmall : "fa fa-check fa-2x fadeInRight animated",
                    timeout : 4000
                });
            }
        });
    },
    restore: function(entityId) {
        $.SmartMessageBox({
                "title": "<i class='fa fa-recycle txt-color-orangeDark'></i> Restore <span class='txt-color-orangeDark'>¿<strong>Está seguro que lo desea restaurar</strong></span>?",
                "buttons": "[No][Yes]"
            }, function(a) {
            "Yes" == a && ($.root_.addClass("animated fadeOutUp"),
            setTimeout(self.restoreAjax(entityId), 1e3))
        })
    },
    deleteAjax: function($a) {
        var self = this;
        var urlDelete = $a.attr('href');
        $.ajax({
            type: 'POST',
            url: urlDelete,
            success: function() {
                $.smallBox({
                    title : "Eliminar",
                    content : "<i class='fa fa-clock-o'></i> <i>El expediente se elimino correctamente.</i>",
                    color : "#659265",
                    iconSmall : "fa fa-check fa-2x fadeInRight animated",
                    timeout : 4000
                });
                dataTable.deleteRow($a.parents('tr'));
            }
        });
    },
    delete: function($a) {
        var self = this;
        var cudap = $a.parent().attr('data-cudap');
        $.SmartMessageBox({
                "title": "<i class='fa fa-trash-o txt-color-orangeDark'></i> Eliminar Expediente <strong>" + cudap + "</strong><br/><span class='txt-color-orangeDark'>¿<strong>Está seguro que desea realizar está operación</strong>?</span>",
                "buttons": "[No][Si]"
            }, function(r) {
                if (r === "Si") {
                    setTimeout(self.deleteAjax($a), 1e3);
                }
        })
    },
    addButtonEvents: function() {
        var self = this;
        $(self.classButtonDelete).each(function() {
            $(this).on('click', function(e) {
                e.preventDefault();
                self.delete($(this));
            });
        });
    },
    init: function(args) {
        var widget = this;
        for (var property in args) {
          if (args.hasOwnProperty(property)) {
            widget[property] = args[property];
          }
        }
        widget.addButtonEvents();
    }
});


var PrestamosWidget = Class.extend({

    updateTotalFin: function() {
        var importe = $("#qc_progvisbundle_prestamos_importe").val();
        var cuotas = $("#qc_progvisbundle_prestamos_cuotas").val();
        var tasa = $("#qc_progvisbundle_prestamos_tasa").val();
        if ($.isNumeric(importe) && $.isNumeric(cuotas) && $.isNumeric(tasa)) {
            importe = parseInt(importe, 10);
            cuotas = parseInt(cuotas, 10);
            tasa = parseInt(tasa, 10);
            var total = (importe * (( cuotas * tasa ) / 100)) + importe;
            $("#qc_progvisbundle_prestamos_totalFinan").val(total);
        }
    },

    addInputEvents: function() {
        var self = this;
        $("#qc_progvisbundle_prestamos_importe").change(function() {
            var value = $(this).val();
            if ($.isNumeric(value)) {
                self.updateTotalFin();
            }
        });
        $("#qc_progvisbundle_prestamos_cuotas").change(function() {
            var value = $(this).val();
            if ($.isNumeric(value)) {
                self.updateTotalFin();
            }
        });
        $("#qc_progvisbundle_prestamos_tasa").change(function() {
            var value = $(this).val();
            if ($.isNumeric(value)) {
                self.updateTotalFin();
            }
        });
    },

    validateDataForCreateCuotas: function(data) {
        if (data['vencimiento'] === "" || !validateDate(data['vencimiento'])) {
            bootbox.alert("Debe ingresar una Fecha de vencimiento Válida.");
            return false;
        }

        if (!data['cuotas']) {
            bootbox.alert("Debe ingresar la cantidad de Cuotas.");
            return false;
        }

        if (!data['totalFinanciado']) {
            bootbox.alert("Para generar las cuotas previamente debe estar generado el Monto Total Financiado. <br/>Para generar el mismo deberá estar ingresada la cantidad de Cuotas, el Importe y la Tasa Mensual.");
            return false;
        }

        return true;
    },

    showCuotasModal: function() {
        var self = this;
        var urlGenerarCuotas = Routing.generate('ajax_prestamos_generar_cuotas');
        var data = {
            'vencimiento' : $("#qc_progvisbundle_prestamos_venciInicial").val(),
            'totalFinanciado' : $("#qc_progvisbundle_prestamos_totalFinan").val(),
            'cuotas' : $("#qc_progvisbundle_prestamos_cuotas").val()
        };
        if (self.validateDataForCreateCuotas(data)) {
            $.post( urlGenerarCuotas, data)
              .done(function( response ) {
                $(response).modal();
            });
        }
    },

    addButtonEvents: function() {
        var self = this;
        $('#show_cuotas').click(function(e) {
            e.preventDefault();
            self.showCuotasModal();
        });
    },

    init: function() {
        this.addInputEvents();
        this.addButtonEvents();
    }
});

var PrestItemWidget = Class.extend({
    prestamoId: '',
    totalAPagar: 0,
    pagar: function() {
        this.ingresarDatosModal();
        $("#pagarCuotas").modal();
    },
    cancelarCompleto: function() {
        this.seleccionarCuotas();
        this.ingresarDatosModal();
        $("#cancelarCompleto").modal();
    },
    seleccionarCuotas: function() {
        $("input.pagar-cuota").each(function() {
            $(this).prop('checked', true);
        });
    },
    ingresarDatosModal: function() {
        var self = this;
        $(".vPrestamoId").val(self.prestamoId);
        var cuotasSeleccionadas = [];
        self.totalAPagar = 0;
        $("input.pagar-cuota:checked").each(function() {
            cuotasSeleccionadas.push($(this).attr('data-cuota'));
            var pagado = parseFloat($(this).attr('data-pagado'));
            pagado = pagado ? pagado.toFixed(2) : 0;
            var importe = parseFloat($(this).attr('data-importe')).toFixed(2);
            self.totalAPagar += importe - pagado;
        });
        $(".hCuotasSeleccionadas").html(cuotasSeleccionadas.join(','));
        $(".vCuotasSeleccionadas").val(cuotasSeleccionadas.join(','));
        self.totalAPagar = self.totalAPagar.toFixed(2);
        $(".hMontoTotalAPagar").html($.number(self.totalAPagar, 2, ',', '.'));
        $(".vMontoTotalAPagar").val(self.totalAPagar);
    },
    cuotasSeleccionadas: function() {
        var self = this;
        var cantCuotasSeleccionadas = $("input.pagar-cuota:checked").length;
        return cantCuotasSeleccionadas;
    },
    clearAlerts: function() {
        $(".alert.not-blank").attr('hidden','hidden');
        $(".alert.less-than").attr('hidden','hidden');
    },
    addInputEvents: function() {
        var self = this;
        $("#btnPagar").click(function(e) {
            self.clearAlerts();
            if (self.cuotasSeleccionadas() !== 0) {
                self.pagar();
            } else {
                bootbox.alert("Debe seleccionar por lo menos una cuota para pagar");
            }
        });
        $("#btnCancelarCompleto").click(function(e) {
            self.cancelarCompleto();
        });
        $("#pagarCuotas form").submit(function(e) {
            $(".alert.not-blank").attr('hidden', true);
            $(".alert.less-than").attr('hidden', true);
            monto = $("#monto").val();
            if (!monto) {
                $(".alert.not-blank").removeAttr('hidden');
                e.preventDefault();
            }
            if (parseFloat(monto) > parseFloat(self.totalAPagar)) {
                $(".alert.less-than").removeAttr('hidden');
                e.preventDefault();
            }
        });
    },
    init: function(args) {
        var self = this;
        for(var a in args) {
            if (args.hasOwnProperty(a)) {
                self[a] = args[a];
            }
        }
        this.addInputEvents();
    }
});

var ChequeWidget = Class.extend({
    urlTasa: '',
    tipoTasaId: '',
    costoImpuestoCheque: 0,
    updateTipoTasa: function() {
        var self = this;
        var cliente_id = $("#qc_progvisbundle_cheque_cliente").val();
        var procedencia = $("#qc_progvisbundle_cheque_procedencia").val();
        if (cliente_id != "" && procedencia != "") {
            $.ajax({
                type: 'post',
                url: self.urlTasa,
                data: {
                    cliente_id: cliente_id,
                    procedencia: procedencia
                },
                success: function(options) {
                    $("#qc_progvisbundle_cheque_tipoTasa").empty();
                    $("#qc_progvisbundle_cheque_tipoTasa").append(options);
                    if (self.tipoTasaId !== "") {
                        $("#qc_progvisbundle_cheque_tipoTasa").val(self.tipoTasaId);
                        self.updateGestionSugerida();
                    }
                }
            });
        } else {
            if (self.tipoTasaId === "") {
                $("#qc_progvisbundle_cheque_tipoTasa").empty();
            }
        }
    },
    updateDate: function(date) {
        var self = this;
        var fechaVencimiento = $("#qc_progvisbundle_cheque_fechaVencimiento").val();
        if (fechaVencimiento !== "") {
            $("#qc_progvisbundle_cheque_dia").val(getDayOfWeek(date));
            $("#qc_progvisbundle_cheque_cantDias").val(diffDays(date));
        }
    },
    updateGestionSugerida() {
        var self = this;
        var tipoTasa = $("#qc_progvisbundle_cheque_tipoTasa").val();
        if (tipoTasa !== "") {
            var percent = $("#qc_progvisbundle_cheque_tipoTasa").find("option:selected").attr('data-percent');
            $("#qc_progvisbundle_cheque_sugerida").val(percent);
            $("#qc_progvisbundle_cheque_gestion").val(percent);
        }
    },
    getDA: function(dias) {
        switch(dias) {
            case "Domingo": return 4;
            case "Sábado": return 5;
            case "Viernes": return 5;
            case "Jueves": return 5;
            case "Miercoles": return 2;
            case "Martes": return 2;
            case "Lunes": return 2;
            default: return 0;
        }
    },
    /**
     * Si la cantidad de días del cheque a cobrar es menor o igual a cero
     * quiere decir que el cheque ya se puede cobrar,
     * entonces devolvemos 1 como para que no aumente el interes
     */
    getDC: function(cantDias) {
        if (cantDias <= 0) {
            return 1;
        }
        return cantDias;
    },
    updateImporteCliente: function() {
        var self = this;
        var cliente = $("#qc_progvisbundle_cheque_cliente").val();
        var importe = $("#qc_progvisbundle_cheque_importeNominal").val();
        var procedencia = $("#qc_progvisbundle_cheque_procedencia").val();
        var fechaVencimiento = $("#qc_progvisbundle_cheque_fechaVencimiento").val();
        var dia = $("#qc_progvisbundle_cheque_dia").val();
        var cantDias = $("#qc_progvisbundle_cheque_cantDias").val();
        var tipoTasa = $("#qc_progvisbundle_cheque_tipoTasa").val();
        var sugerida = $("#qc_progvisbundle_cheque_sugerida").val();
        var gestion = $("#qc_progvisbundle_cheque_gestion").val();
        
        // Deben estar completados todos los campos para calcular el importe a entregar del cliente
        if (cliente !== "" && importe !== "" && procedencia !== "" && fechaVencimiento !== "" && dia !== "" &&
            cantDias !== "" && tipoTasa !== "" && sugerida !== "" && gestion !== "") {

            /* Fórmula para el Cálculo del % Sugerido:  ( TC / 30 ) * ( DC + DA ) + Costo Impuesto Cheque */
            var honorarios = (((gestion / 100) / 30) * (parseInt(self.getDC(cantDias)) + parseInt(self.getDA(dia))) + (self.costoImpuestoCheque/100)) * importe;
            var importeCliente = importe - honorarios;
            $("#qc_progvisbundle_cheque_importeCliente").val(importeCliente.toFixed(2));

        }
    },
    addEventListener: function() {
        var self = this;
        $("#qc_progvisbundle_cheque_fechaVencimiento").on('change', function() {
            self.updateDate($(this).val());
        });
        $("#qc_progvisbundle_cheque_cliente").on('change', function() {
            self.updateTipoTasa();
        });
        $("#qc_progvisbundle_cheque_procedencia").on('change', function() {
            self.updateTipoTasa();
        });
        $("#qc_progvisbundle_cheque_tipoTasa").on('change', function() {
            self.updateGestionSugerida();
        });
        $("select").on('change', function() {
            self.updateImporteCliente();
        });
        $("input").on('change', function() {
            self.updateImporteCliente();
        });
    },
    updateInputs() {
        var self = this;

        self.updateDate($("#qc_progvisbundle_cheque_fechaVencimiento").val());

        self.updateTipoTasa();

        self.updateGestionSugerida();

        self.updateImporteCliente();
    },
    init: function(args) {
        var widget = this;
        for (var property in args) {
          if (args.hasOwnProperty(property)) {
            widget[property] = args[property];
          }
        }
        this.addEventListener();
        this.updateInputs();
    }
});

var AltaMasivaWidget = ChequeWidget.extend({
    addEventListener: function() {
        var self = this;
        $("#qc_progvisbundle_cheque_cliente").on('change', function() {
            self.updateTipoTasa();
        });
        $("#qc_progvisbundle_cheque_procedencia").on('change', function() {
            self.updateTipoTasa();
        });
    },
    init: function(args) {
        var widget = this;
        for (var property in args) {
          if (args.hasOwnProperty(property)) {
            widget[property] = args[property];
          }
        }
        this.addEventListener();
    }
});

var ChequesStandByWidget = Class.extend({
    totalAPagar: 0,
    montoDisponible: 0,
    comprar: function() {
        this.ingresarDatosModal();
        $("#mComprarCheques").modal();
    },
    seleccionarCheques: function() {
        $("input.comprar-cheque").each(function() {
            $(this).prop('checked', true);
        });
    },
    ingresarDatosModal: function() {
        var self = this;
        self.clear();
        var chequesSeleccionados = [];
        var nroChequesSeleccionados = [];
        self.totalAPagar = 0;
        $("input.comprar-cheque:checked").each(function() {
            chequesSeleccionados.push($(this).attr('data-cheque'));
            nroChequesSeleccionados.push($(this).attr('data-numero'));
            var importe = parseFloat($(this).attr('data-importe'));
            self.totalAPagar += importe;
        });
        $(".hChequesSeleccionados").html(nroChequesSeleccionados.join(','));
        $(".vChequesSeleccionados").val(chequesSeleccionados.join(','));
        self.totalAPagar = self.totalAPagar.toFixed(2);
        $(".hMontoTotalAPagar").html($.number(self.totalAPagar, 2, ',', '.'));
        $(".vMontoTotalAPagar").val(self.totalAPagar);
        if (self.montoDisponible < self.totalAPagar) {
            $("#hMontoAPagar").html($.number(self.totalAPagar, 2, ',', '.'));
            $("#hMontoDisponible").html($.number(self.montoDisponible, 2, ',', '.'));
            $(".alert.monto-no-disponible").removeAttr('hidden');
            $("#text-confirm").hide();
            $("#submit").hide();
        }
    },
    chequesSeleccionados: function() {
        var self = this;
        return $("input.comprar-cheque:checked").length;
    },
    clear: function() {
        $(".alert.monto-no-disponible").attr('hidden','hidden');
        $("#text-confirm").show();
        $("#submit").show();
    },
    addInputEvents: function() {
        var self = this;
        $("#btnComprar").click(function(e) {
            if (self.chequesSeleccionados() > 0) {
                self.comprar();
            } else {
                bootbox.alert("Debe seleccionar por lo menos un cheque para comprar");
            }
        });
    },
    init: function(args) {
        var self = this;
        for(var a in args) {
            if (args.hasOwnProperty(a)) {
                self[a] = args[a];
            }
        }
        this.addInputEvents();
    }
});

var ChequesVencidosWidget = Class.extend({
    gestionar: function() {
        this.ingresarDatosModal();
        $("#mGestionarCheques").modal();
    },
    gestionarTipoCheque: function() {
        var tipoTasa = $("#qc_progvisbundle_gestionar_tipo_cheque_tipoTasa").val();
        $("#vTipoTasa").val(tipoTasa);
        $("#mGestionarTipoCheques").modal();
    },
    seleccionarCheques: function() {
        $("input.to-select").each(function() {
            $(this).prop('checked', true);
        });
    },
    ingresarDatosModal: function() {
        var self = this;
        self.clear();
        var chequesSeleccionados = [];
        var nroChequesSeleccionados = [];
        $("input.to-select:checked").each(function() {
            chequesSeleccionados.push($(this).attr('data-cheque'));
            nroChequesSeleccionados.push($(this).attr('data-numero'));
        });
        $(".hChequesSeleccionados").html(nroChequesSeleccionados.join(','));
        $(".vChequesSeleccionados").val(chequesSeleccionados.join(','));
    },
    chequesSeleccionados: function() {
        var self = this;
        return $("input.to-select:checked").length;
    },
    clear: function() {
        $(".alert.monto-no-disponible").attr('hidden','hidden');
        $("#text-confirm").show();
        $("#submit").show();
    },
    updateByTipoTasa: function() {
        if ($("#qc_progvisbundle_gestionar_tipo_cheque_tipoTasa").val() !== "") {
            $("#dataTable").dataTable().fnFilter($("#qc_progvisbundle_gestionar_tipo_cheque_tipoTasa").find(':selected').html());
        } else {
            $("#dataTable").dataTable().fnFilter("");
        }
    },
    addInputEvents: function() {
        var self = this;

        $("#qc_progvisbundle_gestionar_tipo_cheque_tipoTasa").on('change', function(){
            self.updateByTipoTasa();
        });
        self.updateByTipoTasa();

        $("#btnGestionar").click(function(e) {
            if (self.chequesSeleccionados() > 0) {
                self.gestionar();
            } else {
                bootbox.alert("Debe seleccionar por lo menos un cheque para gestionar");
            }
        });

        $("#btnGestionarTipoCheque").click(function(e) {
            if ($("#qc_progvisbundle_gestionar_tipo_cheque_tipoTasa").val() === "") {
                bootbox.alert("Debe seleccionar un Tipo de Cheque para continuar");
                e.preventDefault();
                return false;
            }
            if ($("#dataTable tbody").find('tr[role=row]').length === 0) {
                bootbox.alert("No hay cheques disponibles para gestionar");
                e.preventDefault();
                return false;
            }
            self.gestionarTipoCheque();
        });
    },
    init: function(args) {
        var self = this;
        for(var a in args) {
            if (args.hasOwnProperty(a)) {
                self[a] = args[a];
            }
        }
        this.addInputEvents();
    }
});

var ChequesEnGestionWidget = Class.extend({
    confirmar: function() {
        this.ingresarDatosModal();
        $("#mConfirmarCheques").modal();
    },
    seleccionarCheques: function() {
        $("input.to-select").each(function() {
            $(this).prop('checked', true);
        });
    },
    ingresarDatosModal: function() {
        var self = this;
        self.clear();
        var chequesSeleccionados = [];
        var nroChequesSeleccionados = [];
        $("input.to-select:checked").each(function() {
            chequesSeleccionados.push($(this).attr('data-cheque'));
            nroChequesSeleccionados.push($(this).attr('data-numero'));
        });
        $(".hChequesSeleccionados").html(nroChequesSeleccionados.join(','));
        $(".vChequesSeleccionados").val(chequesSeleccionados.join(','));
    },
    chequesSeleccionados: function() {
        var self = this;
        return $("input.to-select:checked").length;
    },
    clear: function() {
        $("#text-confirm").show();
        $("#submit").show();
    },
    addInputEvents: function() {
        var self = this;

        $("#btnConfirmar").click(function(e) {
            if (self.chequesSeleccionados() > 0) {
                self.confirmar();
            } else {
                bootbox.alert("Debe seleccionar por lo menos un cheque para confirmar");
            }
        });
    },
    init: function(args) {
        var self = this;
        for(var a in args) {
            if (args.hasOwnProperty(a)) {
                self[a] = args[a];
            }
        }
        this.addInputEvents();
    }
});
