
/**
 * Valida que la fecha ingresada sea una fecha válida.
 * El formato de la fecha debe ser Y-m-d
 *
 * @param  string value
 *
 * @return boolean
 */
function validateDate(value) {
    var split = value.split("-");
    var y = parseInt(split[0], 10),
        m = parseInt(split[1], 10),
        d = parseInt(split[2], 10);
    var date = new Date(y, m - 1, d);

    if (date.getFullYear() === y && date.getMonth() + 1 == m && date.getDate() == d) {
      return true;
    } else {
      return false;
    }
}

/**
 * Obtiene el día de la semana a partir de una fecha string con formato 'Y-m-d'
 *
 * @param  string value
 *
 * @return string Día de la semana
 */
function getDayOfWeek(value) {
    var datetime = getDate(value);
    var dow = datetime.getDay();
    switch(dow) {
        case (0): return "Domingo";
        case (1): return "Lunes";
        case (2): return "Martes";
        case (3): return "Miercoles";
        case (4): return "Jueves";
        case (5): return "Viernes";
        case (6): return "Sábado";
        default: return "";
    }
}

/**
 * Obtiene el date a traves de un string con formato 'Y-m-d'
 *
 * @param    string value
 *
 * @return date
 */
function getDate(value) {
    var date = value.split("-");
    var y = parseInt(date[0], 10),
        m = parseInt(date[1], 10),
        d = parseInt(date[2], 10);

    var date = new Date(y, m - 1, d);

    return date;
}

/**
 * Devuelve la diferencia de una fecha con la fecha de hoy
 *
 * @param  string value Fecha con formato Y-m-d
 *
 * @return integer
 */
function diffDays(value) {
    var date1 = new Date();
    var date2 = getDate(value);
    //var timeDiff = Math.abs(date2.getTime() - date1.getTime()); Obtiene la difencia y lo convierte en positivo
    var timeDiff = date2.getTime() - date1.getTime();
    var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));

    return diffDays;
}