<?php

namespace UserBundle\Controller;

use UserBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * User controller.
 *
 * @Route("user")
 */
class UserController extends Controller
{
    /**
     * Lists all user entities.
     *
     * @Route("/", name="user_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('UserBundle:User')->findAll();

        return $this->render('user/index.html.twig', array(
            'entities' => $entities,
        ));
    }

    /**
     * Creates a new user entity.
     *
     * @Route("/new", name="user_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $entity = new User();
        $form = $this->createForm('UserBundle\Form\UserType', $entity);
        $form->handleRequest($request);
        $flashbagService = $this->get('flashbag_service');
        $translator = $this->get('translator');

        if ($form->isSubmitted()) {
            $em = $this->getDoctrine()->getManager();

            $username = $entity->getUsername();    
            $entity->setUsername(strtolower($username));    
            
            $em->persist($entity);
            $em->flush($entity);

            $flashbagService->setFlash('success', $translator->trans('core.controller.messages.creation_success', [], 'CoreBundle'));
            return $this->redirectToRoute('user_show', array('id' => $entity->getId()));
        }

        return $this->render('user/new.html.twig', array(
            'user' => $entity,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a user entity.
     *
     * @Route("/{id}", name="user_show")
     * @Method("GET")
     */
    public function showAction(User $entity)
    {
        $deleteForm = $this->createDeleteForm($entity);

        return $this->render('user/show.html.twig', array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing user entity.
     *
     * @Route("/{id}/edit", name="user_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, User $entity)
    {
        $deleteForm = $this->createDeleteForm($entity);
        $editForm = $this->createForm('UserBundle\Form\UserType', $entity);
        $editForm->handleRequest($request);
        $flashbagService = $this->get('flashbag_service');
        $translator = $this->get('translator');

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $flashbagService->setFlash('success', $translator->trans('core.controller.messages.update_success', [], 'CoreBundle'));
            return $this->redirectToRoute('user_edit', array('id' => $entity->getId()));
        }

        return $this->render('user/edit.html.twig', array(
            'user' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a user entity.
     *
     * @Route("/{id}", name="user_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, User $user)
    {
        $form = $this->createDeleteForm($user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($user);
            $em->flush();
        }

        return $this->redirectToRoute('user_index');
    }

    /**
     * Creates a form to delete a user entity.
     *
     * @param User $user The user entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(User $user)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('user_delete', array('id' => $user->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
