<?php

namespace UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Role\RoleHierarchy;

use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\DataTransformerInterface;

class UserType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', TextType::class, [
                'label' => 'Usuario',
                'label_attr' => ['icon' => 'user'],
                'attr' => ['width' => 'col-sm-12 col-lg-6']
            ])
            ->add('email', EmailType::class, [
                'label' => 'Email',
                'label_attr' => ['icon' => 'envelope-o'],
                'attr' => ['width' => 'col-sm-12 col-lg-6']
            ])
            ->add('firstname', TextType::class, [
                'label' => 'Nombre',
                'label_attr' => ['icon' => 'user'],
                'required' => false,
                'attr' => ['width' => 'col-sm-12 col-lg-6']
            ])
            ->add('lastname', TextType::class, [
                'label' => 'Apellido',
                'label_attr' => ['icon' => 'user'],
                'required' => false,
                'attr' => ['width' => 'col-sm-12 col-lg-6']
            ])
            ->add('roles', ChoiceType::class, [
                 'choices' => array(
                                     'Rol Admin' => 'ROLE_SUPER_ADMIN',
                                     ),
                'choices_as_values' => true, // switched
                'label' => 'Roles',
                'label_attr' => ['icon' => 'user'],
                'attr' => [ 'width' => 'col-sm-12 col-lg-12'],
                'expanded' => true,
                'multiple' => true,
                'required' => true,
            ])


            ->add('plainPassword', RepeatedType::class, array(
                'type' => PasswordType::class,
                'options' => array('translation_domain' => 'FOSUserBundle'),
                'first_options' => array('label' => 'form.password'),
                'second_options' => array('label' => 'form.password_confirmation'),
                'invalid_message' => 'Debe Ingresar una Password Correcta',
                'required' => false
            ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'userbundle_user';
    }


}
